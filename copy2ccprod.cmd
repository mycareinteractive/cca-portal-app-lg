call compile.cmd

for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "TIMESTAMP=%dt:~0,12%"

call robocopy \\10.88.152.198\f$\Aceso\Avon\apps\cca-portal-app-lg \\10.88.152.198\f$\Aceso\Avon\apps\_backups\cca-portal-app-lg.%TIMESTAMP% /E
call robocopy .\dist \\10.88.152.198\f$\Aceso\Avon\apps\cca-portal-app-lg /E

exit 0