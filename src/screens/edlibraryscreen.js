import Pelican from 'pelican';
require('../css/edlibrary.css');
var template = require('../templates/edlibraryscreen.hbs');

const EdLibraryScreen = Pelican.Screen.extend({

    className: 'edlibrary edtitles',

    template: template,

    keyEvents: {},

    events: {
        'click .back-button': 'back',
        'click .folder': 'selectFolder'
    },

    widgets: {
        library: {
            widgetClass: Pelican.ContentMenu,
            selector: '#menu',
            options: {}
        }
    },

    onInit: function (options) {
        var slug = this.path.replace('home/', App.upserver.baseSlug);
        console.log('Slug: ' + this.path);

        var self = this;

        // get self content
        var parent = new Pelican.Models.Content();
        parent.fetch({
            data: {
                slug: slug
            }
        }).done(function () {
            self.$('.page-title').first().html(parent.toJSON().content.metadatas.title);
        });

        var folders = new Pelican.Collections.ContentCollection();
        folders.fetch({
            data: {
                slug: slug,
                depth: 1
            }
        })
            .done(function () {
                var content = folders.toJSON();
                self.getWidget('library').model.set('folders', content);
                self.getWidget('library').selectFirstFolder();
            });
    },

    onAttach: function () {
        this.getWidget('library').selectFirstFolder();
    },

    selectFolder: function (e) {
        var children = null;
        var $obj = $(e.target);
        var slug = $obj.attr('data-slug');

        var self = this;

        var assets = new Pelican.Collections.ContentCollection();
        assets.fetch({
            data: {
                slug: slug,
                depth: 1
            }
        })
            .done(function () {
                var content = assets.toJSON();
                self.getWidget('library').model.set('assets', content);
            });
    }
});

export default EdLibraryScreen;