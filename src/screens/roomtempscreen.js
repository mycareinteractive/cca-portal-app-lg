import Pelican from 'pelican';
import i18next from 'i18next';

require('../css/roomtemp.css');
var template = require('../templates/roomtempscreen.hbs');

const RoomTempScreen = Pelican.Screen.extend({

    className: 'roomtemp',

    template: template,

    regions: {
        'title': {
            selector: '.page-title'
        }
    },

    keyEvents: {},

    tempStatusHtml: "<div id='temp-status'>"
        +   "<span id='temp-value'></span>"
        +   "<span id='temp-label'>Current Temperature</span>"
        + "</div>",

    events: {
        'click .back-button': 'back',
        'click .button3.sub-page-next': 'changeTemperature'
    },

    widgets: {
        menu: {
            widgetClass: Pelican.TabMenu,
            selector: '#menu'
        }
    },

    getTemperature: function(){
        var defer = jQuery.Deferred();
        App.upserver.api('/opc', 'GET', {
            "machineName": App.config.opc.machineName,
            "serverClass": App.config.opc.serverClass,
            "itemId": App.data.device.deviceProvision.location.metadatas['hvac.roomTemp']
        })
            .done(function(data){
                defer.resolve(data);
            })
            .fail(function(err){
                defer.reject(err)
            });

        return defer;
    },

    setTemperature: function(temp){
        App.upserver.api('/opc', 'PUT', {
            "machineName": App.config.opc.machineName,
            "serverClass": App.config.opc.serverClass,
            "itemId": App.data.device.deviceProvision.location.metadatas['hvac.setPoint'],
            "value": temp
        });

        this.showTemperatureNotification(temp);
    },
    formatTemperature: function(temp){
        var rtemp = Math.round(parseInt(temp));

        return rtemp;
    },

    showTemperatureNotification: function(temp){
        var self =  this;
        self.$('#temp-notification-value').html(temp + "&#176");
        self.$('#temp-notification').show();

        $.doTimeout(15000, function(){
            self.$('#temp-notification').hide();
        });
    },

    setTemperatureLabel: function(temp){
        var self = this;
        self.$('#temp-value').html(temp + "&#176");
    },

    checkTemperature: function(){
        var self = this;
        $.doTimeout('tempCheck', 60000, function(){
            self.getTemperature()
                .done(function(temp){
                    var t = self.formatTemperature(temp);
                    self.setTemperatureLabel(t)
                    console.log('current temp of the room is: ' + t);
                });
        });
    },

    changeTemperature: function(){
        var temp = self.$('.menu-tab-button.selected').attr('data-current-slug');
        console.log('A request has been made to change the temperature to ' + temp);
        this.setTemperature(parseInt(temp));
    },

    highlightAdjustTemp: function(e) {
        var self = this;
        var temp = self.$(e.target).attr('data-current-slug');

        self.$('.sub-content').html('<div class="sub-text1"><p><br><br>The temperature of your room has been adjusted to ' + temp + '.<br><br>Please allow a few minutes for the change in room temperature to take place. </p> </div>');
    },

    onInit: function (options) {
        var slug = this.path.replace('home/', App.upserver.baseSlug);
        console.log('Slug: ' + slug);

        var self = this;

        // get self content
        var parent = new Pelican.Models.Content();
        parent.fetch({
            data: {
                slug: slug
            }
        }).done(function () {
            var pcont = parent.toJSON();
            self.getWidget('menu').model.set({
                parent: pcont.content
            });
            var htext = i18next.t("Set your Room Temperature, then press Select.");
            self.$('.page-title').first().html(htext);
        });


        var contents = new Pelican.Collections.ContentCollection();
        contents.fetch({
            data: {
                slug: slug,
                depth: 2
            }
        })
            .done(function () {
                console.log('APP ROUTE SEGMENT: ' + slug);
                var scont = contents.toJSON();
                var model = {menu: scont};
                self.getWidget('menu').model.set(model);
                self.focus('.back-button');

            });

        self.getTemperature()
            .done(function(temp){
                var t = self.formatTemperature(temp);
                self.setTemperatureLabel(t);
                self.$('#temp-status').show();
                console.log('current temp of the room is: ' + t);
            });

    },

    onScreenShow: function(){
        this.checkTemperature();
        // this.$(".major").prepend(this.tempStatusHtml);
    },

    onScreenHide: function(){
        console.log('On screen hide.');
        $.doTimeout('tempCheck');
    },

    onAttach: function () {
    }

});

export default RoomTempScreen;