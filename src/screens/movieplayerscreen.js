import VideoPlayerScreen from './videoplayerscreen';

require('../css/dialog.css');

const MoviePlayerScreen = VideoPlayerScreen.extend({

    autoStart: false,
    bookmarking: true,
    allowFastforward: true,
    allowRewind: true,
    analytics: true,

    creditLength: 600000,

    type: 'movie',

    bookmarkIds: [],
    renditions: [],

    onInit: function (options) {
        var self = this;

        $('body').addClass('video');

        console.log('THESE ARE THE OPTIONS');
        console.log(options);

        // the 3 lines below should always be run
        self._setParams();
        self._initializeMediaEvents();

        self.getVideo()
            .done(function () {
                if (self.video) {
                    self.bookmarkIds = self.video.bookmarkIds;
                    self.duration = self.video.metadatas.runtime * 1000 || 0;
                }

                if (self.video && self.video.renditions) {
                    self.renditions = self.video.renditions;
                }
                self.chooseRendition();
            });
    },

    chooseRendition: function () {
        var self = this;
        if (!self.renditions || self.renditions.length < 1) {
            console.log('ERROR: no rendition to play!');
            return;
        }

        if (self.renditions.length > 1) {
            // TODO: display a dialog for language selection here
            self.vurl = self.getDefaultUrl();
            self.startVideo();
        }
        else {
            self.vurl = self.getDefaultUrl();
            self.startVideo();
        }
    },

    getDefaultUrl: function () {
        var self = this;
        var vobj = self.video.renditions[0];
        var vurl = vobj.metadatas.url || vobj.metadatas.originalUrl;
        if(App.config.originalUrl)
        {
            vurl = vobj.metadatas.originalUrl;
        }

        return vurl;
    },

    onPlayStart: function () {
        this.updateBookmark('start', this.startPosition);
        this._startBookmarking(20000, 'ping');
    },

    onPlayEnd: function () {
        if (!this.duration) {
            // if Razuna doesn't have video length, when we reaches end, we assume this is the length
            this.duration = this.position + 1000;
        }
    },

    onStop: function () {
        this.updateBookmark('stop');
        this._stopBookmarking();
        this.back();
    }

});

export default MoviePlayerScreen;