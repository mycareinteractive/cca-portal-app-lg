import Pelican from 'pelican';
import EducationVideoPlayerScreen from './educationvideoplayerscreen';
import VideoPlayerScreen from './videoplayerscreen';
import i18next from 'i18next';

require('../css/dialog.css');
var template = require('../templates/epicvideoplayerscreen.hbs');

const EpicVideoPlayerScreen = EducationVideoPlayerScreen.extend({

    template: template,

    questionLocale: 'en',

    keyEvents: function () {
        return $.extend({}, VideoPlayerScreen.prototype.keyEvents, {
            //'MENU': 'showComprehension'
        });
    },

    widgets: {
        dialog: {
            widgetClass: Pelican.Dialog,
            selector: '#dialog',
            options: {hidden: true}
        }
    },

    events: {
        'focus #spanish': 'highlightSpanish',
        'focus #english': 'highlightEnglish',
        'click .lang-button': 'selectRendition',
        'click .checkboxButton': 'selectLearner',
        'click #next': 'selectNext',
        'click #yes': 'selectYes',
        'click #no': 'selectNo',
        'click #close': 'sendResults'
    },

    _t: function (msg, lang) {
        lang = lang || this.questionLocale;
        return i18next.t(msg, {lng: lang});
    },

    chooseRendition: function () {

        var self = this;
        if (self.queries && self.queries['bookmarkId']) {
            self.bookmarkId = self.queries['bookmarkId'];
        }
        else if (self.bookmarkIds) { // if bookmarkId is not passed in from URL, use the one from EducationVideoPlayer
            self.bookmarkId = self.bookmarkIds[0];
        }

        self.epicResults = [];
        self.epicResults.renditionlang = 'english';
        self.epicResults.learners = [];
        self.epicResults.completion = 0;
        self.epicResults.understanding = 'X';

        if (!self.renditions || self.renditions.length <= 0) {
            console.log('ERROR: no rendition to play!');
            return;
        }
        console.log('self renditions');
        console.log(self.renditions);
        if (self.renditions.length >= 2) {
            // TODO: display a dialog for language selection here
            self.showRendition();
        }
        else {
            self.showLearner();
        }
    },

    onPlayEnd: function () {
        if (!this.duration) {
            // if Razuna doesn't have video length, when we reaches end, we assume this is the length
            this.duration = this.position + 1000;
        }
        ;
    },

    onStop: function () {
        var self = this;
        this.updateBookmark('stop');
        var creditLength = this.creditLength;
        this.log('onStop, position: ' + self.position + ', duration: ' + self.duration);
        if (Math.floor(this.position) + creditLength > this.duration) {
            self.epicResults.completion = 'Complete';
            this.showComprehension();
        } else {
            self.epicResults.completion = 'Incomplete';
            this.sendResults();
        }


    },

    highlightEnglish: function () {
        this.$('#instruction div').hide();
        this.$('#instruction div.english').show();
    },

    highlightSpanish: function () {
        this.$('#instruction div').hide();
        this.$('#instruction div.spanish').show();
    },

    showRendition: function () {
        var p1 = 'Would you like to view this educational content in English or Spanish?';
        var p2 = '';
        var p3 = 'Please select a language.';

        var html = (
            '<div class="english">' +
            '<p class="text4">' + this._t(p1) + '</p><br>' +
            '<p class="text2"><br>' + this._t(p2) + '<br><br></p>' +
            '<p class="text1">' + this._t(p3) + '</p>' +
            '</div>' +
            '<div class="spanish" style="display:none">' +
            '<p class="text4">' + this._t(p1, 'es') + '</p><br>' +
            '<p class="text2"><br>' + this._t(p2, 'es') + '<br><br></p>' +
            '<p class="text1">' + this._t(p3, 'es') + '</p>' +
            '</div>'
        );

        var model = {
            buttons: [
                {id: 'english', text: 'English', className: 'lang-button'},
                {id: 'spanish', text: 'Español', className: 'lang-button'}
            ],
            content: html
        };

        this.getWidget('dialog').show();
        this.getWidget('dialog').model.set(model);
        this.focus('#english');
    },

    showLearner: function () {
        var p1 = 'Who is viewing this educational content?';
        var p2 = 'Please select all that apply.';
        var p3 = 'Use your arrow keys to Select who is viewing, then select Next.';

        var html = (
            '<div>' +
            '<p class="text4">' + this._t(p1) + '<br>' + this._t(p2) + '</p><br>' +
            '<p class="text2"><br></p>' +
            '<p class="text1">' + this._t(p3) + '</p>' +
            '</div>'
        );

        var model = {
            buttons: [
                {id: 'patient', text: this._t('The Patient'), className: 'checkboxButton'},
                {id: 'family', text: this._t('Family Member(s)'), className: 'checkboxButton'},
                {id: 'caregiver', text: this._t('Caregiver(s)'), className: 'checkboxButton'},
                {id: 'next', text: this._t('Next')}
            ],
            content: html
        };

        this.getWidget('dialog').show();
        this.getWidget('dialog').model.set(model);
        this.focus('#patient');

    },

    selectRendition: function (e) {
        var self = this;
        self.epicResults.renditionlang = $(e.target).attr('id');

        // set question language
        switch (self.epicResults.renditionlang) {
            case 'english':
                self.questionLocale = 'en';
                break;
            case 'spanish':
                self.questionLocale = 'es';
                break;
            default:
                self.questionLocale = 'en';
                break;
        }

        self.showLearner();
    },

    getUrl: function () {
        var self = this;
        var vurl = '';

        $.each(self.video.renditions, function (i, rendition) {
            console.log(rendition.type);

            if (rendition.type == 'Original') { // this is the default url if no language matches
                if (App.config.originalUrl) {
                    vurl = rendition.metadatas.originalUrl || rendition.metadatas.url;
                } else {
                    vurl = rendition.metadatas.url || rendition.metadatas.originalUrl;
                }
            }
            var lang = (rendition.metadatas.language || 'english').toLowerCase();
            console.log(rendition.metadatas.originalUrl);
            if (lang == self.epicResults.renditionlang) {
                if (App.config.originalUrl) {
                    vurl = rendition.metadatas.originalUrl || rendition.metadatas.url;
                } else {
                    vurl = rendition.metadatas.url || rendition.metadatas.originalUrl;
                }
                return false;
            }
        });

        return vurl;
    },

    showComprehension: function () {
        var p1 = 'Did you understand the information in this educational content?';
        var p2 = 'If you have questions, answer No and someone will discuss your questions with you.';
        var p3 = 'For help, ask a member of your care team or press the Nurse button.';

        var html = (
            '<div>' +
            '<p class="text4">' + this._t(p1) + '</p><br>' +
            '<p class="text2"><br>' + this._t(p2) + '<br><br></p>' +
            '<p class="text1">' + this._t(p3) + '</p>' +
            '</div>'
        );

        var model = {
            buttons: [
                {id: 'yes', text: this._t('Yes, I understand'), className: 'button'},
                {id: 'no', text: this._t('No, I have questions'), className: 'button'}

            ],
            content: html
        };

        this.epicResults.understanding = 'NR';
        this.getWidget('dialog').show();
        this.getWidget('dialog').model.set(model);
        this.focus('#yes');

    },

    showConfirmation: function () {
        var p1 = 'Thank you';
        var p2 = 'for your feedback.';
        var p3 = 'There is no such thing as a bad question. Our goal is to your understanding of your health and treatment. Someone will discuss your questions with you. ';
        var p4 = 'Press Select to close this window and return Home.';

        var html = (
            '<div>' +
            '<p class="text4">' + this._t(p1) + '<br>' + this._t(p2) + '</p><br>' +
            '<p class="text2"><br>' + this._t(p3) + '<br><br></p>' +
            '<p class="text1">' + this._t(p4) + '</p>' +
            '</div>'
        );

        var model = {
            buttons: [
                {id: 'close', text: this._t('Close'), className: 'button'}
            ],
            content: html
        };

        this.getWidget('dialog').show();
        this.getWidget('dialog').model.set(model);
        this.focus('#close');

    },

    showNoReponse: function () {
        var p1 = 'Did you understand the information in this educational content?';
        var p2 = '';
        var p3 = 'If you have questions, answer No and someone will discuss your questions with you.';

        var html = (
            '<div>' +
            '<p class="text4">' + this._t(p1) + '</p><br>' +
            '<p class="text2"><br>' + this._t(p2) + '<br><br></p>' +
            '<p class="text1">' + this._t(p3) + '</p>' +
            '</div>'
        );

        var model = {
            buttons: [
                {id: 'yes', text: this._t('Yes, I understand'), className: 'learnerButton'},
                {id: 'no', text: this._t('No, I have questions'), className: 'learnerButton'}

            ],
            content: html
        };

        this.getWidget('dialog').show();
        this.getWidget('dialog').model.set(model);
        this.focus('#yes');
    },

    selectNext: function (e) {
        var self = this;
        self.vurl = self.getUrl();
        var learners = this.$('.checked');
        self.epicResults.learners = $.map(learners, function (l) {
            return $(l).attr('id');
        });

        if (self.epicResults.learners.length >= 1) {
            this.getWidget('dialog').hide();
            //to do fix radio
            var learners = this.$('.checked');
            console.log(self.epicResults.learners.length);
            self.startVideo();
        }
    },

    selectYes: function () {
        var self = this;
        this.showConfirmation();
        self.epicResults.understanding = 'VU';
    },

    selectNo: function () {
        var self = this;
        this.showConfirmation();
        self.epicResults.understanding = 'RC';
    },


    sendResults: function () {
        var self = this;
        console.log('epicResults');
        console.log(self.epicResults);
        if (this.position >= 1)
            upserver.api('/me/education', 'POST',
                {
                    "bookmarkId": self.bookmarkId,
                    "language": self.epicResults.renditionlang,
                    "learner": self.epicResults.learners.join(','),
                    "completion": self.epicResults.completion,
                    "understanding": self.epicResults.understanding
                });
        this.back();
    },

    selectLearner: function (e) {
        console.log($(e.target).hasClass('checked'));
        if ($(e.target).hasClass('checked'))
            $(e.target).removeClass('checked');
        else
            $(e.target).addClass('checked');

    }
});

export default EpicVideoPlayerScreen;