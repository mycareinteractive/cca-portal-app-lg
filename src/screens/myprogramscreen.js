import Pelican from 'pelican';
require('../css/common.css');
require('../css/edlibrary.css');
var template = require('../templates/myprogramscreen.hbs');

const MyProgramScreen = Pelican.Screen.extend({

    className: 'myprogram edtitles',

    template: template,

    keyEvents: {},

    events: {
        'click .back-button': 'back'
    },

    widgets: {
        bookmarks: {
            widgetClass: Pelican.ContentButtonGroup,
            selector: '#bookmarks',
            options: {nonActive: true, backButton: true}
        }
    },

    onInit: function (options) {
        var slug = this.path.replace('home/', App.upserver.baseSlug);
        console.log('Slug: ' + this.path);
        var self = this;

        var parent = new Pelican.Models.Content();
        parent.fetch({
            data: {
                slug: slug
            }
        }).done(function () {
            self.$('.page-title').first().html(parent.toJSON().content.metadatas.title);
        });

        var bookmarks = new Pelican.Collections.BookmarkCollection();
        bookmarks.fetch({ data: { type: 'education'} })
            .done(function () {
                // some bookmarks might have invalid content (maybe no providerAssetId found, not content not uploaded yet)
                // we need to filter these bookmarks out
                var bmark = $.grep(bookmarks.toJSON(), function(b, i){
                    return (b.content);
                });

                var bmod = {buttons: bmark};
                self.getWidget('bookmarks').model.set(bmod);
                self.getWidget('bookmarks').selectFirst();
            });
    },

    onAttach: function () {
        this.getWidget('bookmarks').selectFirst();
    }
});

export default MyProgramScreen;