import Pelican from 'pelican';

require('../css/webpage.css');

var template = require('../templates/webpagescreen.hbs');

const WebPageScreen = Pelican.Screen.extend({

    className: 'webpage',

    template: template,

    keyEvents: {
        'UP': 'pageUp',
        'DOWN': 'pageDown',
        'PGUP': 'pageUp',
        'PGDN': 'pageDown'
    },

    events: {},

    onAttach: function () {
        var self = this;

        self.pageUrl = this.queries['url'];
        self.pageWidth = parseInt(this.queries['width']) || 1280;
        self.pageHeight = parseInt(this.queries['height']) || 640 * 2;

        if (self.pageUrl) {
            self.pageUrl = decodeURI(self.pageUrl);
            self.$('#mask').show();

            self.$('#page-iframe').attr('src', self.pageUrl);

            self.$('#page-iframe').load(function () {
                self.$('#page-iframe').height(self.pageHeight + 100);
                self.$('#mask').hide();
                self.focusMainFrame();
            });

            // a timer just in case the loading takes forever
            $.doTimeout('iframe loading timer', 10000, function () {
                self.$('#mask').hide();
                return false;
            });

            // set a timer to grab focus every second
            self.focusMainFrame();
            $.doTimeout('iframe focus', 1000, self.focusMainFrame.bind(self));
        }
    },

    onScreenHide: function() {
        // kill focus timer
        $.doTimeout('iframe focus');
    },

    focusMainFrame: function () {
        window.focus();
        return true;
    },

    pageUp: function () {
        var pos = this.$('#frame-wrapper').scrollTop();
        pos = (pos < 500) ? 0 : pos - 500;
        this.$('#frame-wrapper').stop().animate({scrollTop: pos}, '500');
        return true;
    },

    pageDown: function () {
        var pos = this.$('#frame-wrapper').scrollTop();
        pos += 500;
        this.$('#frame-wrapper').stop().animate({scrollTop: pos}, '500');
        return true;
    }

});

export default WebPageScreen;