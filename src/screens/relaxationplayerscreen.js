import VideoPlayerScreen from './videoplayerscreen';

require('../css/dialog.css');

const RelaxationPlayerScreen = VideoPlayerScreen.extend({

    videoParam: {
        repeatCount: 999  // infinite loop
    },

    autoStart: true,
    bookmarking: false,
    allowFastforward: false,
    allowRewind: false,
    allowPause: false,
    analytics: true,

    creditLength: 1000,

    type: 'relaxation',

    bookmarkIds: [],
    renditions: [],

    onInit: function (options) {
        VideoPlayerScreen.prototype.onInit.call(this, options);
    },

    onPlayStart: function () {
        // when start calculate the appropriate start position
        this.calculateStartPosition();
    },

    onPlayEnd: function () {
        // when relaxation video reaches end, replay
        // this.seek(0);
        return true;    // return true so videoplayer screen will NOT stop the stream
    },

    onStop: function () {
        this.back();
    },

    calculateStartPosition: function () {
        this.duration = this.duration || 1800000;   // If duration not available use 30 minutes as default
        // Pretend 'channel' starts midnight, calculate what position should we be at this moment
        var now = new Date();
        var then = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0);
        var diff = now.getTime() - then.getTime();  // milliseconds since midnight
        this.startPosition = diff % this.duration;
    }

});

export default RelaxationPlayerScreen;